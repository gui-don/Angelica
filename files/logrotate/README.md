## Logrotate files ##

<p align="center"><img src="../../ange.png" /></p>

Logrotate configurations must be placed here.

These files will be copied only if logrotate is enabled (`logrotate.enabled`).

Sometimes, you’ll need to copy script only in specific environment, like *web* servers in *local*.
In this case, put your scripts inside a `local/web` directory in the current folder.
Another example, if you want to copy a configuration in all *prod* servers, put your script a `prod` directory.
For all *db* servers, inside `db`, etc.

Simply name your logrotate configuration files describing the targeted log.
The configuration files must end with `.conf` here, even if it is removed when copied on servers.
For instance, you can name `application` a logrotate configuration which aims at rotating an application logs.

Side note: all configurations here will be copied in the proper logrotate configuration directory with a `ansible-` prefix.
