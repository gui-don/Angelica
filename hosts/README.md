## Hosts ##

<p align="center"><img src="../ange.png" /></p>

Here are ansible hosts files. See the [intro for inventory file](https://docs.ansible.com/ansible/intro_inventory.html).

First thing, copy *hosts/env.dist* to your corresponding environment:


- *prod* if you’re setting up production servers.

- *staging* if you’re setting up staging servers.

- *local* if you’re setting up local (or development) servers.

Everytime you start an ansible playbook, you must designate the inventory file (via the `-i` argument) corresponding to the environment you are setting up.

Note that the *env.dist* files is a model file to inspire you.

**Important**: use hostnames instead of IPs. You can use your local `/etc/hosts` if DNS are not set.

