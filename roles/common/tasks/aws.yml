---
- name: AWS - Install needed package - aws-cli
  apt: name=awscli state=latest update_cache=yes

- name: AWS - Add the AWS group
  group: name={{ aws.group }} state=present system=no

- name: AWS - Add .aws for every AWS-allowed user
  file: path=/home/{{ item[0].name }}/.aws state=directory owner={{ item[0].name }} group={{ item[0].name }} mode=0755
  when: "item[0].name is defined and item[1].name is defined and aws.group in item[0].groups"
  with_nested:
    - "{{ users | default({}) }}"
    - "{{ aws.profiles | default({}) }}"

- name: AWS - Remove all AWS config files for every user
  file: path="/home/{{ item.name }}/.aws/config" state=absent
  when: item.name is defined and aws.reset
  with_items:
    - "{{ users | default({}) }}"

- name: AWS - Remove all AWS credential files for every user
  file: path="/home/{{ item.name }}/.aws/credentials" state=absent
  when: item.name is defined and aws.reset
  with_items:
    - "{{ users | default({}) }}"

- name: AWS - Copy all profile region inside config files for every user
  blockinfile:
    create: yes
    owner: "{{ item[0].name }}"
    group: "{{ item[0].name }}"
    mode: 0644
    path: "/home/{{ item[0].name }}/.aws/config"
    state: present
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ item[1].name }}"
    block: |
      [profile {{ item[1].name }}]
      region = {{ item[1].region }}
  when: "item[0].name is defined and item[1].name is defined and aws.group in item[0].groups"
  with_nested:
    - "{{ users | default({}) }}"
    - "{{ aws.profiles | default({}) }}"

- name: AWS - Copy all profiles inside credentials files for every user
  blockinfile:
    create: yes
    owner: "{{ item[0].name }}"
    group: "{{ item[0].name }}"
    mode: 0600
    path: "/home/{{ item[0].name }}/.aws/credentials"
    state: present
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ item[1].name }}"
    block: |
      [{{ item[1].name }}]
      aws_access_key_id = {{ item[1].user }}
      aws_secret_access_key = {{ item[1].password }}
  when: "item[0].name is defined and item[1].name is defined and aws.group in item[0].groups"
  with_nested:
    - "{{ users | default({}) }}"
    - "{{ aws.profiles | default({}) }}"
