## Vars vault files ##

<p align="center"><img src="../../ange.png" /></p>

Here comes all the protected variables files. [See ansible vault documentation here](https://docs.ansible.com/ansible/latest/vault.html).

You must create one vault file by environment (same name as your inventory files) and define (at least) the sudo password: **this is the only mandatory password**.

Copy the *env.dist* file and rename it to one of your environment (e.g. *prod*). Look at the structure inside the dist file to get things righe.

Every vault encrypted vars comes after the `_vault` key.

To create an encrypted password, [look at the documentation](https://docs.ansible.com/ansible/latest/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml). Here is an example:

- Run:

        $ ansible-vault encrypt-string --ask-vault-pass

- Enter the password;
- *CTRL + D*;
- Copy all the encrypted string (beginning with a `!vault |`);
- Paste the encrypted string to a vault variable file.
